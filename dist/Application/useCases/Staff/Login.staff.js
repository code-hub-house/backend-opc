"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginStaffUseCase = void 0;
const Staff_mapper_1 = require("@application/mappers/Staff.mapper");
const Staff_1 = require("@domain/entities/Staff");
const Unauthorized_1 = require("@domain/exceptions/Unauthorized");
const UserNotFound_1 = require("@domain/exceptions/UserNotFound");
class LoginStaffUseCase {
    constructor(staffRepository) {
        this.staffRepository = staffRepository;
    }
    async run(email, password) {
        const staff = await this.staffRepository.getByEmail(email);
        if (staff === undefined || staff === null)
            throw new UserNotFound_1.UserNotFound('User not found');
        const staffInstace = (0, Staff_1.getStaffInstance)(staff);
        if (staffInstace === undefined || staffInstace === null)
            throw new UserNotFound_1.UserNotFound('User not found');
        const isPasswordCorrect = await staffInstace.verifyPassword(password);
        if (!isPasswordCorrect)
            throw new Unauthorized_1.Unathorized('Password is incorrect');
        const staffLogged = Staff_mapper_1.StaffMapperDto.domainToDto(staffInstace);
        return staffLogged;
    }
}
exports.LoginStaffUseCase = LoginStaffUseCase;
