"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EditUserStaffCase = void 0;
const Staff_mapper_1 = require("@application/mappers/Staff.mapper");
const UserNotFound_1 = require("@domain/exceptions/UserNotFound");
const StaffValidations_1 = require("@domain/services/StaffValidations");
class EditUserStaffCase {
    constructor(staffRepository) {
        this.staffRepository = staffRepository;
    }
    async run(props) {
        const staff = await this.staffRepository.get(props.dni);
        if (staff === undefined || staff === null)
            throw new UserNotFound_1.UserNotFound('User not found');
        const staffValidations = new StaffValidations_1.StaffValidations(this.staffRepository);
        await staffValidations.runValidations(staff);
        const staffUpdated = await this.staffRepository.edit(staff);
        return Staff_mapper_1.StaffMapperDto.domainToDto(staffUpdated);
    }
}
exports.EditUserStaffCase = EditUserStaffCase;
