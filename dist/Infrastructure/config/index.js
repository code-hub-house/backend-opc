"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
dotenv_1.default.config({
    path: path_1.default.resolve(__dirname, `../../../${(_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : 'development'}.env`),
});
console.log(__dirname);
const config = {
    NODE_ENV: process.env.NODE_ENV,
    PORT: process.env.PORT,
};
console.log(config);
exports.default = config;
