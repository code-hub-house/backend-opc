"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OPCApp = void 0;
const config_1 = __importDefault(require("@infrastructure/config/config"));
const logger_1 = require("@infrastructure/driven-adapters/logger");
const Server_1 = require("./Server");
class OPCApp {
    async start() {
        var _a;
        this.port = (_a = config_1.default.PORT) !== null && _a !== void 0 ? _a : '2426';
        const logger = new logger_1.LoggerService();
        logger.log('\nenvironment variables:', config_1.default);
        this.server = new Server_1.Server(this.port, logger);
        return await this.server.listen();
    }
    async stop() {
        var _a;
        return await ((_a = this.server) === null || _a === void 0 ? void 0 : _a.stop());
    }
}
exports.OPCApp = OPCApp;
