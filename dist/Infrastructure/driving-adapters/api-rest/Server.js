"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const express_1 = __importDefault(require("express"));
const routes_1 = __importDefault(require("./routes"));
class Server {
    constructor(port, logger) {
        this._port = port;
        this._app = (0, express_1.default)();
        this.logger = logger;
        this._app.use(express_1.default.json());
        this._app.use(express_1.default.urlencoded({ extended: false }));
        this._app.use(routes_1.default);
    }
    async listen() {
        return await new Promise(resolve => {
            this._httpServer = this._app.listen(this._port, () => {
                this.logger.debug(`Mock OPC App is running at http://localhost:${this._port}`);
                this.logger.debug('Press CTRL-C to stop\n');
                resolve();
            });
        });
    }
    async stop() {
        return await new Promise((resolve, reject) => {
            if (this._httpServer !== null && this._httpServer !== undefined) {
                this._httpServer.close(error => {
                    if (error !== null && error !== undefined) {
                        return reject(error);
                    }
                    return resolve();
                });
            }
            return resolve();
        });
    }
}
exports.Server = Server;
