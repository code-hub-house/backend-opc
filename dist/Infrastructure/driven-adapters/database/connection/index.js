"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgresConnection = void 0;
// import { LoggerService } from '@infrastructure/driven-adapters/logger';
const logger_1 = require("@infrastructure/driven-adapters/logger");
const connection_1 = require("./connection");
class PostgresConnection {
    constructor() {
        const logger = new logger_1.LoggerService();
        this.client = connection_1.client;
        this.pool = connection_1.pool;
        void this.connectPool().then(() => logger.log('Connected to database'));
    }
    static getInstance() {
        if (PostgresConnection.instance === undefined || PostgresConnection.instance === null) {
            PostgresConnection.instance = new PostgresConnection();
        }
        return PostgresConnection.instance;
    }
    async connectPool() {
        var _a;
        await ((_a = this.pool) === null || _a === void 0 ? void 0 : _a.connect());
    }
    async connect() {
        var _a;
        await ((_a = this.client) === null || _a === void 0 ? void 0 : _a.connect());
    }
    async desconnect() {
        var _a;
        await ((_a = this.client) === null || _a === void 0 ? void 0 : _a.end());
    }
    async queryClient(query, params) {
        var _a, _b;
        if (params === undefined || params === null) {
            return await ((_a = this.client) === null || _a === void 0 ? void 0 : _a.query(query));
        }
        return await ((_b = this.client) === null || _b === void 0 ? void 0 : _b.query(query, params));
    }
    async query(query, params) {
        var _a, _b;
        if (params === undefined || params === null) {
            return await ((_a = this.pool) === null || _a === void 0 ? void 0 : _a.query(query));
        }
        return await ((_b = this.pool) === null || _b === void 0 ? void 0 : _b.query(query, params));
    }
}
exports.PostgresConnection = PostgresConnection;
