"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserMapper = void 0;
const Staff_1 = require("@domain/entities/Staff");
exports.UserMapper = {
    toDomain(userDao) {
        const staff = {
            dni: userDao.dni,
            name: userDao.name,
            email: userDao.email,
            cellphone: userDao.cellphone,
            password: userDao.password,
            roleCode: userDao.role
        };
        return (0, Staff_1.getStaffInstance)(staff);
    },
    toDomainList(userDaoList) {
        return userDaoList.map(userDao => exports.UserMapper.toDomain(userDao));
    },
    toPersistence(staff) {
        return {
            dni: staff.dni,
            name: staff.name,
            email: staff.email,
            cellphone: staff.cellphone,
            password: staff.password,
            role: staff.roleCode
        };
    },
    toPersistenceList(staffList) {
        return staffList.map(staff => this.toPersistence(staff));
    }
};
