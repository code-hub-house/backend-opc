"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getStaffInstance = void 0;
const Role_1 = require("@domain/enums/Role");
const AdminExternal_1 = require("./AdminExternal");
const AdminTeacher_1 = require("./AdminTeacher");
const OPCAssistant_1 = require("./OPCAssistant");
const OPCCoordinator_1 = require("./OPCCoordinator");
const getStaffInstance = (props) => {
    switch (Role_1.roleCode[props.roleCode]) {
        case Role_1.roleCode.OPCAssistant:
            return new OPCAssistant_1.OPCAssistant(props);
        case Role_1.roleCode.OPCCoordinator:
            return new OPCCoordinator_1.OPCCoordinator(props);
        case Role_1.roleCode.ADMINExternal:
            return new AdminExternal_1.AdminExternal(props);
        case Role_1.roleCode.ADMINTeacher:
            return new AdminTeacher_1.AdminTeacher(props);
    }
    return undefined;
};
exports.getStaffInstance = getStaffInstance;
