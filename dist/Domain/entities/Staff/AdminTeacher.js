"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminTeacher = void 0;
const StaffAdministrative_1 = require("./StaffAdministrative");
class AdminTeacher extends StaffAdministrative_1.StaffAdministrative {
    constructor(props) {
        super(props);
    }
}
exports.AdminTeacher = AdminTeacher;
