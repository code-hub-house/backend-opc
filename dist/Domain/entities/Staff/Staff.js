"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Staff = void 0;
class Staff {
    constructor(props) {
        this.dni = props.dni;
        this.name = props.name;
        this.email = props.email;
        this.cellphone = props.cellphone;
        this.password = props.password;
        this.roleCode = props.roleCode;
    }
    verifyPassword(password) {
        return this.password === password;
    }
}
exports.Staff = Staff;
