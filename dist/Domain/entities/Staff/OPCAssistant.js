"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OPCAssistant = void 0;
const StaffOPC_1 = require("./StaffOPC");
class OPCAssistant extends StaffOPC_1.StaffOPC {
    constructor(props) {
        super(props);
    }
}
exports.OPCAssistant = OPCAssistant;
