"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OPCCoordinator = void 0;
const StaffOPC_1 = require("./StaffOPC");
class OPCCoordinator extends StaffOPC_1.StaffOPC {
    constructor(props) {
        super(props);
    }
}
exports.OPCCoordinator = OPCCoordinator;
