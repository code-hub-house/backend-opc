"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminExternal = void 0;
const StaffAdministrative_1 = require("./StaffAdministrative");
class AdminExternal extends StaffAdministrative_1.StaffAdministrative {
    constructor(props) {
        super(props);
    }
}
exports.AdminExternal = AdminExternal;
