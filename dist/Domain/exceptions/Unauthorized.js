"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Unathorized = void 0;
const _1 = require(".");
class Unathorized extends _1.Exception {
    constructor(message) {
        super(message);
        this.name = 'Unathorized';
        this.spanishMessage = 'No autorizado';
    }
}
exports.Unathorized = Unathorized;
