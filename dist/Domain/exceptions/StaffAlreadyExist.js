"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffAlreadyExist = void 0;
const _1 = require(".");
class StaffAlreadyExist extends _1.Exception {
    constructor(message) {
        super(message);
        this.name = 'StaffAlreadyExist';
        this.spanishMessage = 'El personal ya existe';
    }
}
exports.StaffAlreadyExist = StaffAlreadyExist;
