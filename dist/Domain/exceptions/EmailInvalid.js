"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailInvalid = void 0;
const _1 = require(".");
class EmailInvalid extends _1.Exception {
    constructor(email) {
        super(`The email ${email} is invalid`);
        this.name = 'EmailInvalid';
        this.spanishMessage = 'El email no es válido';
        this.message = `The email ${email} is invalid`;
    }
}
exports.EmailInvalid = EmailInvalid;
