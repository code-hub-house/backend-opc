"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exception = void 0;
/* eslint-disable @typescript-eslint/no-useless-constructor */
class Exception extends Error {
    constructor(message) {
        super(message);
    }
}
exports.Exception = Exception;
