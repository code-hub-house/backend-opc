"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserNotFound = void 0;
const _1 = require(".");
class UserNotFound extends _1.Exception {
    constructor(message) {
        super(message);
        this.name = 'UserNotFound';
        this.spanishMessage = 'Usuario no encontrado';
    }
}
exports.UserNotFound = UserNotFound;
