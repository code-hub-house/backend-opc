"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffValidations = void 0;
const Role_1 = require("@domain/enums/Role");
const EmailInvalid_1 = require("@domain/exceptions/EmailInvalid");
const passwordInvalid_1 = require("@domain/exceptions/passwordInvalid");
const RoleCodeError_1 = require("@domain/exceptions/RoleCodeError");
const StaffAlreadyExist_1 = require("@domain/exceptions/StaffAlreadyExist");
class StaffValidations {
    constructor(staffRepository) {
        this.staffRepository = staffRepository;
    }
    async runValidations(staff) {
        await this.validateDni(staff.dni);
        await this.validateEmail(staff.email);
        this.validatePassword(staff.password);
        this.validateEmailFormat(staff.email);
        this.validateCellphone(staff.cellphone);
        this.validateRole(Role_1.roleCode[staff.roleCode]);
    }
    async validateDni(dni) {
        const staff = await this.staffRepository.get(dni);
        if (staff !== undefined) {
            throw new StaffAlreadyExist_1.StaffAlreadyExist('Staff with this DNI already exists');
        }
    }
    async validateEmail(email) {
        const staff = await this.staffRepository.getByEmail(email);
        if (staff !== undefined) {
            throw new StaffAlreadyExist_1.StaffAlreadyExist('Staff with this email already exists');
        }
    }
    validatePassword(password) {
        if (password.length < 8) {
            throw new passwordInvalid_1.PasswordInvalid('Password must be at least 8 characters long');
        }
        if (!/[A-Z]/.test(password)) {
            throw new passwordInvalid_1.PasswordInvalid('Password must contain at least one uppercase letter');
        }
        if (!/[a-z]/.test(password)) {
            throw new passwordInvalid_1.PasswordInvalid('Password must contain at least one lowercase letter');
        }
        if (!/[0-9]/.test(password)) {
            throw new passwordInvalid_1.PasswordInvalid('Password must contain at least one number');
        }
    }
    validateEmailFormat(email) {
        if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
            throw new EmailInvalid_1.EmailInvalid('Email format is not valid');
        }
    }
    validateRole(rc) {
        const roles = Object.values(Role_1.roleCode);
        if (!roles.includes(rc)) {
            throw new RoleCodeError_1.RoleCodeError('Role code is not valid');
        }
    }
    validateCellphone(cellphone) {
        if (!/^\d+$/.test(cellphone)) {
            throw new RoleCodeError_1.RoleCodeError('Cellphone is not valid');
        }
    }
}
exports.StaffValidations = StaffValidations;
