"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("@infrastructure/driven-adapters/database/connection");
describe('PostgresConnection', () => {
    it('should connect to database', async () => {
        const postgresConnection = connection_1.PostgresConnection.getInstance();
        await postgresConnection.connect().then(() => {
            expect(postgresConnection).toBeDefined();
        });
        await postgresConnection.desconnect();
    });
    it('should connect to database with pool', async () => {
        const postgresConnection = connection_1.PostgresConnection.getInstance();
        await postgresConnection.query('SELECT NOW()').then((res) => {
            expect(res).toBeDefined();
        });
    });
    it('should connect to database with client', async () => {
        const postgresConnection = connection_1.PostgresConnection.getInstance();
        await postgresConnection.queryClient('SELECT NOW()').then((res) => {
            expect(res).toBeDefined();
        });
        await postgresConnection.desconnect();
    });
    it('should connect to database with client and params', async () => {
        const postgresConnection = connection_1.PostgresConnection.getInstance();
        await postgresConnection.queryClient('SELECT $1::text as message', ['Hello world!']).then((res) => {
            expect(res).toBeDefined();
        });
        await postgresConnection.desconnect();
    });
    it('should connect to database with pool and params', async () => {
        const postgresConnection = connection_1.PostgresConnection.getInstance();
        await postgresConnection.query('SELECT $1::text as message', ['Hello world!']).then((res) => {
            expect(res).toBeDefined();
        });
    });
});
