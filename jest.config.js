module.exports = {
  roots: [
    '<rootDir>/src'
  ],
  testMatch: [
    '**/__tests__/**/*.+(ts|tsx|js)',
    '**/?(*.)+(spec|test).+(ts|tsx|js)'
  ],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  moduleNameMapper: {
    '@domain/(.*)': '<rootDir>/src/Domain/$1',
    '@application/(.*)': '<rootDir>/src/Application/$1',
    '@infrastructure/(.*)': '<rootDir>/src/Infrastructure/$1'
  }
};
