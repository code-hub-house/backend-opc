export enum roleCode {
  SUPERADMIN = 'SUPERADMIN',
  'opc-coordinator' = 'opc-coordinator',
  'opc-assistant' = 'opc-assistant',
  'admin-professor' = 'admin-professor',
  'admin-external' = 'admin-external',
}
