import { StaffProps } from '@domain/interfaces/StaffProps';

export interface StaffRepository {
  save: (staff: StaffProps) => Promise<StaffProps>
  edit: (staff: StaffProps) => Promise<StaffProps>
  get: (dni: string) => Promise<StaffProps | undefined>
  getByEmail: (email: string) => Promise<StaffProps | undefined>
  login: (email: string, password: string) => Promise<StaffProps | undefined>
}
