import { Product } from '@domain/entities/products/products';

export interface ProductRepository {
  findByName: (name: string) => Promise<Product | undefined>
  findAll: () => Promise<Product[]>
  findById: (id: string) => Promise<Product | undefined>
  create: (product: Product) => Promise<Product>
  update: (product: Product) => Promise<Product>
  delete: (id: string) => Promise<Product>
}
