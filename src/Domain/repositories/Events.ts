import { Event } from '@domain/entities/events/events';

export interface EventsRepository {
  getEvents: () => Promise<Event[]>
  getEventById: (id: string) => Promise<Event | undefined>
  createEvent: (event: Event) => Promise<Event | undefined>
  updateEvent: (event: Event) => Promise<Event | undefined>
  deleteEvent: (id: string) => Promise<Event | undefined>
  getEventsByDni: (dni: string) => Promise<Event[]>
}
