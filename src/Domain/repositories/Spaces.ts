import { Space } from '@domain/entities/space/Space';

export interface SpaceRepository {
  create: (data: Space) => Promise<Space>
  update: (data: Space) => Promise<Space>
  delete: (id: string) => Promise<Space>
  findByName: (name: string) => Promise<Space | undefined>
  findById: (id: string) => Promise<Space | undefined>
  findAll: () => Promise<Space[]>
}
