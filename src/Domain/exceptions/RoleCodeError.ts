import { Exception } from '.';

export class RoleCodeError extends Exception {
  constructor (message?: string) {
    super(message);
    this.name = 'RoleCodeError';
    this.spanishMessage = 'El código de rol no es válido';
    this.message = message ?? this.spanishMessage;
  }
}
