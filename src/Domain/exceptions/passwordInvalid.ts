import { Exception } from '.';

export class PasswordInvalid extends Exception {
  constructor (password: string) {
    super(`The password ${password} is invalid`);
    this.name = 'PasswordInvalid';
    this.spanishMessage = 'La contraseña no es válida';
    this.message = `The password ${password} is invalid`;
  }
}
