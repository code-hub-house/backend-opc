import { Exception } from '.';

export class InvalidReservation extends Exception {
  constructor (message: string) {
    super(message);
  }
}
