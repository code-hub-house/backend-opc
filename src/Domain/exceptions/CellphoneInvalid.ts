import { Exception } from '.';

export class CellphoneInvalid extends Exception {
  constructor (message: string) {
    super(message);
    this.name = 'CellphoneInvalid';
    this.spanishMessage = 'Celular inválido';
  }
}
