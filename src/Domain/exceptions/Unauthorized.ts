import { Exception } from '.';

export class Unathorized extends Exception {
  constructor (message: string) {
    super(message);
    this.name = 'Unathorized';
    this.spanishMessage = 'No autorizado';
  }
}
