import { Exception } from '.';

export class UserNotFound extends Exception {
  constructor (message: string) {
    super(message);
    this.name = 'UserNotFound';
    this.spanishMessage = 'Usuario no encontrado';
  }
}
