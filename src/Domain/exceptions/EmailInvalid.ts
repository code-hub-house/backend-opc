import { Exception } from '.';

export class EmailInvalid extends Exception {
  constructor (email: string) {
    super(`The email ${email} is invalid`);
    this.name = 'EmailInvalid';
    this.spanishMessage = 'El email no es válido';
    this.message = `The email ${email} is invalid`;
  }
}
