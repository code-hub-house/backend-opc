# Capa de dominio de la aplicación
Esta capa contiene las entidades y los servicios de la aplicación. Los servicios son los que se encargan de la lógica de negocio de la aplicación.

## Entidades
Las entidades son los objetos que representan los datos de la aplicación. Estas entidades son las que se utilizan para la comunicación entre las capas de la aplicación.

## Repositorios
Los repositorios son los que se encargan de la comunicación con la base de datos. Estos repositorios se encargan de la comunicación entre las capas de la aplicación.

### Diagrama de clases
[DrawIO](https://app.diagrams.net/#G1GB1UObhvMdUfq1BJ33oSfECTUZOIOOmM)