import { StaffProps } from '@domain/interfaces/StaffProps';
import { Staff } from './Staff';

export class SuperAdmin extends Staff {
  constructor (props: StaffProps) {
    super(props);
  }
}
