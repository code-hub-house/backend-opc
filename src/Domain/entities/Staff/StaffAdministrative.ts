import { StaffProps } from '@domain/interfaces/StaffProps';
import { Staff } from './Staff';

export abstract class StaffAdministrative extends Staff {
  constructor (props: StaffProps) {
    super(props);
  }
}
