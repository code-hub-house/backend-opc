import { StaffProps } from '@domain/interfaces/StaffProps';
import { StaffOPC } from './StaffOPC';

export class OPCCoordinator extends StaffOPC {
  constructor (props: StaffProps) {
    super(props);
  }
}
