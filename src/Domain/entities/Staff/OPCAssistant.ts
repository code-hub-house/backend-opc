import { StaffProps } from '@domain/interfaces/StaffProps';
import { StaffOPC } from './StaffOPC';

export class OPCAssistant extends StaffOPC {
  constructor (props: StaffProps) {
    super(props);
  }
}
