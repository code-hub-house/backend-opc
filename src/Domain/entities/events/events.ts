export interface Event {
  id: string
  name: string
  date: Date
  description: string
  staff_id: string
}
