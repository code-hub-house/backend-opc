import { roleCode } from '@domain/enums/Role';

export interface StaffProps {
  readonly dni: string
  name: string
  email: string
  cellphone: string
  password: string
  roleCode: keyof typeof roleCode
  token?: string
}
