import { ProductDTO } from '@application/dto/Products.dto';
import { Product } from '@domain/entities/products/products';

export const ProductMapper = {
  toDTO: (product: Product): ProductDTO => ({
    id: product.id,
    name: product.name
  }),
  toEntity: (productDTO: ProductDTO): Product => ({
    id: productDTO.id ?? '',
    name: productDTO.name
  }),
  toDTOList: (products: Product[]): ProductDTO[] => {
    return products.map((product) => ProductMapper.toDTO(product));
  },
  toEntityList: (productsDTO: ProductDTO[]): Product[] => {
    return productsDTO.map((product) => ProductMapper.toEntity(product));
  }
};
