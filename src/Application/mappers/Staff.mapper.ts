import { StaffDto, StaffDtoList } from '@application/dto/Staff.dto';
import { getStaffInstance } from '@domain/entities/Staff';
import { Staff } from '@domain/entities/Staff/Staff';
import { StaffProps } from '@domain/interfaces/StaffProps';

export const StaffMapperDto = {
  domainToDto: (staff: Staff | StaffProps): StaffDto => {
    return {
      dni: staff.dni,
      name: staff.name,
      email: staff.email,
      cellphone: staff.cellphone,
      password: staff.password,
      role: staff.roleCode,
      token: staff.token
    };
  },
  domainListToDtoList: (staffList: Staff[]): StaffDto[] => {
    return staffList.map((staff) => StaffMapperDto.domainToDto(staff));
  },
  toDomain: (StaffDto: StaffDto): Staff | undefined => {
    const staff: StaffProps = {
      dni: StaffDto.dni,
      name: StaffDto.name,
      email: StaffDto.email,
      cellphone: StaffDto.cellphone,
      password: StaffDto.password,
      roleCode: StaffDto.role,
      token: StaffDto.token
    };
    return getStaffInstance(staff);
  },
  toDomainList: (StaffDtoList: StaffDtoList): Staff[] => {
    return StaffDtoList.users.map((StaffDto) => StaffMapperDto.toDomain(StaffDto) as Staff);
  }
};
