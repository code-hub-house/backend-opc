import { EventDto } from '@application/dto/Event.dto';
import { Event } from '@domain/entities/events/events';

export const EventsMapper = {
  toDTO: (event: Event): EventDto => ({
    id: event.id,
    name: event.name,
    date: event.date,
    description: event.description,
    user_id: event.staff_id
  }),
  toEntity: (event: EventDto): Event => ({
    id: event.id ?? '',
    name: event.name,
    description: event.description,
    date: event.date,
    staff_id: event.user_id
  }),
  toDTOList: (events: Event[]): EventDto[] => {
    return events.map((event) => EventsMapper.toDTO(event));
  },
  toEntityList: (events: EventDto[]): Event[] => {
    return events.map((event) => EventsMapper.toEntity(event));
  }
};
