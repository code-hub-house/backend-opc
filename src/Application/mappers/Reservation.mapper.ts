import { ReservationDto } from '@application/dto/Reservation.dto';
import { Reservation } from '@domain/entities/reservation/reservation';

export const ReservationMapper = {
  toEntity: (dto: ReservationDto): Reservation => {
    return new Reservation({
      id: dto.id ?? new Date().toISOString(),
      event_id: dto.event.id as string,
      space_id: dto.space,
      ticket: {
        id: dto.id ?? '',
        cost_center: dto.cost_center,
        overtime: dto.overtime,
        overtime_description: dto.overtime_description,
        dependence: dto.dependence,
        state: dto.status ?? 'PENDING'
      }
    });
  },
  toDto: (entity: Reservation): ReservationDto => {
    return {
      id: entity.id,
      event: entity.event,
      space: entity.space.name,
      cost_center: entity.ticket.cost_center,
      overtime: entity.ticket.overtime,
      overtime_description: entity.ticket.overtime_description,
      dependence: entity.ticket.dependence,
      status: entity.ticket.state
    };
  },
  toDtoList: (entities: Reservation[]): ReservationDto[] => {
    return entities.map((entity) => ReservationMapper.toDto(entity));
  },
  toEntityList: (dtos: ReservationDto[]): Reservation[] => {
    return dtos.map((dto) => ReservationMapper.toEntity(dto));
  }
};
