import { SpaceDTO } from '@application/dto/Space.dto';
import { Space } from '@domain/entities/space/Space';

export const SpaceMapper = {
  toDTO: (space: Space): SpaceDTO => {
    return {
      id: space.id,
      name: space.name,
      capacity: space.capacity,
      number: space.number,
      state: space.state
    };
  },
  toEntity: (space: SpaceDTO): Space => {
    return {
      id: space.id ?? '',
      name: space.name,
      capacity: space.capacity,
      number: space.number,
      state: space.state ?? 'active'
    };
  },
  toDTOList: (spaces: Space[]): SpaceDTO[] => {
    return spaces.map((space) => SpaceMapper.toDTO(space));
  },
  toEntityList: (spaces: SpaceDTO[]): Space[] => {
    return spaces.map((space) => SpaceMapper.toEntity(space));
  }
};
