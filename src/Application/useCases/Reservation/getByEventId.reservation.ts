import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class GetByEventIdUseCase {
  constructor (
    private readonly repository: ReservationRepository
  ) { }

  async run (eventId: string): Promise<ReservationDto[]> {
    const reservations = await this.repository.getReservationByEventId(eventId);
    if (reservations === undefined || reservations === null) throw new Error('Reservations not found');
    const reservationsDto = ReservationMapper.toDtoList(reservations);
    return reservationsDto;
  }
}
