import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class GetAllReservationUseCase {
  constructor (
    private readonly reservationRepository: ReservationRepository
  ) {}

  async run (): Promise<ReservationDto[] | null | undefined> {
    const reservations = await this.reservationRepository.getAllReservations();
    if (reservations === null || reservations === undefined) {
      throw new Error('Reservation not found');
    }
    const reservationsDTO = ReservationMapper.toDtoList(reservations);
    return reservationsDTO;
  }
}
