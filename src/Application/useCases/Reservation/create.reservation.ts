import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { EventsRepository } from '@domain/repositories/Events';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class CreateReservationUseCase {
  constructor (private readonly repository: ReservationRepository, private readonly eventsRepository: EventsRepository) {}

  async run (props: ReservationDto): Promise<any> {
    const event = await this.eventsRepository.getEventById(props.event.id as string);
    if (event === undefined || event === null) throw new Error('Event not found');
    const reservationEntity = ReservationMapper.toEntity(props);
    const reservation = await this.repository.createReservation(reservationEntity);
    if (reservation === undefined || reservation === null) throw new Error('Error creating reservation');
    return ReservationMapper.toDto(reservation);
  }
}
