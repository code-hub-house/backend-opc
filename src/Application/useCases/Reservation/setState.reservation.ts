import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { State } from '@domain/enums/State';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class SetStateReservationUseCase {
  private readonly reservationRepository: ReservationRepository;

  constructor (reservationRepository: ReservationRepository) {
    this.reservationRepository = reservationRepository;
  }

  async run (id: string, state: string): Promise<ReservationDto> {
    const reservation = await this.reservationRepository.getReservationById(id);
    if (reservation === undefined || reservation === null) throw new Error('Reservation not found');
    const reservationDto = ReservationMapper.toDto(reservation);
    reservationDto.status = state as keyof typeof State;
    const reservationUpdated = await this.reservationRepository.updateReservation(reservation);
    if (reservationUpdated === undefined || reservationUpdated === null) throw new Error('Error updating reservation');
    return ReservationMapper.toDto(reservationUpdated);
  }
}
