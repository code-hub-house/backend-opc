import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class getBySpaceIdUseCase {
  constructor (
    private readonly reservationRepository: ReservationRepository
  ) {}

  async run (spaceId: string): Promise<ReservationDto[]> {
    const reservations = await this.reservationRepository.getReservationBySpaceId(spaceId);
    if (reservations === undefined || reservations === null) throw new Error('Reservations not found');
    if (reservations.length === 0) throw new Error('Reservations not found');
    const reservationsDto = ReservationMapper.toDtoList(reservations);
    return reservationsDto;
  }
}
