import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { Exception } from '@domain/exceptions';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class GetReservationByUserDniUseCase {
  constructor (private readonly reservationRepository: ReservationRepository) {}

  async run (userDni: string): Promise<ReservationDto[]> {
    const reservations = await this.reservationRepository.getReservationsByUser(userDni);
    if (reservations === null || reservations === undefined) {
      throw new Exception('Reservation not found');
    }
    const reservationsDTO = ReservationMapper.toDtoList(reservations);
    return reservationsDTO;
  }
}
