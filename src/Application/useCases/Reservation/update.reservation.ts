import { ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { State } from '@domain/enums/State';
import { Exception } from '@domain/exceptions';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class UpdateReservationUseCase {
  constructor (private readonly repository: ReservationRepository) {}

  async run (props: ReservationDto): Promise<ReservationDto> {
    if (props.status === undefined || props.status === null) {
      throw new Exception('State is required');
    }
    if (State.TOMODIFY !== props.status) {
      throw new Exception('State is not valid');
    }
    if (props.id === undefined || props.id === null) throw new Exception('Reservation id not found');
    const reservationExists = await this.repository.getReservationById(props.id);
    if (reservationExists === undefined || reservationExists === null) throw new Exception('Reservation not found');
    const reservationEntity = ReservationMapper.toEntity(props);
    const reservation = await this.repository.updateReservation(reservationEntity);
    if (reservation === undefined || reservation === null) throw new Error('Error updating reservation');
    return ReservationMapper.toDto(reservation);
  }
}
