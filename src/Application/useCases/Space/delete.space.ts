import { SpaceRepository } from '@domain/repositories/Spaces';

export class DeleteSpaceUseCase {
  constructor (
    private readonly spacesRepository: SpaceRepository
  ) {}

  async execute (id: string): Promise<void> {
    const space = await this.spacesRepository.findById(id);
    if (space === undefined || space === null) throw new Error('Space not found');
    const deleted = await this.spacesRepository.delete(id);
    if (deleted === undefined || deleted === null) throw new Error('Error deleting space');
  }
}
