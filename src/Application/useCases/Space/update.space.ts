import { SpaceDTO } from '@application/dto/Space.dto';
import { SpaceMapper } from '@application/mappers/space.mapper';
import { SpaceRepository } from '@domain/repositories/Spaces';

export class UpdateSpaceUseCase {
  constructor (
    private readonly spaceRepository: SpaceRepository
  ) { }

  async execute (data: SpaceDTO): Promise<SpaceDTO> {
    if (data.id === undefined) throw new Error('Space id is required');
    const space = await this.spaceRepository.findById(data.id);
    if (space === undefined || space === null) throw new Error('Space not found');
    const spaceEntity = SpaceMapper.toEntity(data);
    const updated = await this.spaceRepository.update(spaceEntity);
    if (updated === undefined || updated === null) throw new Error('Error updating space');
    const spaceDto = SpaceMapper.toDTO(updated);
    return spaceDto;
  }
}
