import { SpaceDTO } from '@application/dto/Space.dto';
import { SpaceMapper } from '@application/mappers/space.mapper';
import { SpaceRepository } from '@domain/repositories/Spaces';

export class CreateSpaceUseCase {
  constructor (private readonly repository: SpaceRepository) {}

  async run (space: SpaceDTO): Promise<SpaceDTO> {
    const spaceEntity = SpaceMapper.toEntity(space);
    const spaceCreated = await this.repository.create(spaceEntity);
    return SpaceMapper.toDTO(spaceCreated);
  }
}
