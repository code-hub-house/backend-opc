import { SpaceDTO } from '@application/dto/Space.dto';
import { SpaceMapper } from '@application/mappers/space.mapper';
import { SpaceRepository } from '@domain/repositories/Spaces';

export class GetAllSpaceUseCase {
  constructor (private readonly spaceRepository: SpaceRepository) {}

  async run (): Promise<SpaceDTO[]> {
    const spaces = await this.spaceRepository.findAll();
    return SpaceMapper.toDTOList(spaces);
  }
}
