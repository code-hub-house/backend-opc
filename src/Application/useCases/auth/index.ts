import { LoginDto, LoginResponseDto } from '@application/dto/login.dto';
import { getStaffInstance } from '@domain/entities/Staff';
import { roleCode } from '@domain/enums/Role';
import { Unathorized } from '@domain/exceptions/Unauthorized';
import { UserNotFound } from '@domain/exceptions/UserNotFound';
import { StaffRepository } from '@domain/repositories/Staff';
import { StaffValidations } from '@domain/services/StaffValidations';
import { JWT } from '@infrastructure/driven-adapters/jwt';
import { Bcrypt } from '@infrastructure/driven-adapters/jwt/Bcrypt';

export class Auth {
  constructor (
    private readonly jwt: JWT,
    private readonly bcrypt: Bcrypt,
    private readonly userRepository: StaffRepository
  ) {}

  async login ({ email, password }: LoginDto): Promise<LoginResponseDto> {
    const user = await this.userRepository.getByEmail(email);
    const staffValidations = new StaffValidations(this.userRepository);
    await staffValidations.validateEmailFormat(email);
    if (user === undefined || user === null) throw new UserNotFound('User not found');
    const staffInstace = getStaffInstance(user);
    if (staffInstace === undefined || staffInstace === null) throw new UserNotFound('User not found');
    const isPasswordValid = await this.bcrypt.compare(password, user.password);
    console.log(isPasswordValid);
    if (!isPasswordValid) throw new Unathorized('Password is incorrect');
    if (!isPasswordValid) {
      throw new Unathorized('Invalid credentials');
    }
    const token = await this.jwt.sign({ user_id: user.dni, email, role: roleCode[user.roleCode] as string, name: user.name, cellphone: user.cellphone });
    return { token };
  }
}
