import { ProductDTO } from '@application/dto/Products.dto';
import { ProductMapper } from '@application/mappers/Product.mapper';
import { ProductRepository } from '@domain/repositories/Products';

export class GetProductByIdUseCase {
  constructor (
    private readonly productRepository: ProductRepository
  ) {}

  async execute (id: string): Promise<ProductDTO> {
    const product = await this.productRepository.findById(id);
    if (product === undefined) throw new Error('Product not found');
    const productDTO = ProductMapper.toDTO(product);
    return productDTO;
  }
}
