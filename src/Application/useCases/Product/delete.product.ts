import { ProductRepository } from '@domain/repositories/Products';

export class DeleteProductUseCase {
  private readonly productRepository: ProductRepository;

  constructor (productRepository: ProductRepository) {
    this.productRepository = productRepository;
  }

  async execute (id: string): Promise<void> {
    const product = await this.productRepository.findById(id);
    if (product === undefined) throw new Error('Product not found');
    await this.productRepository.delete(id);
  }
}
