import { ProductDTO } from '@application/dto/Products.dto';
import { ProductMapper } from '@application/mappers/Product.mapper';
import { ProductRepository } from '@domain/repositories/Products';

export class GetProductByNameUseCase {
  constructor (
    private readonly productsRepository: ProductRepository
  ) {}

  async execute (name: string): Promise<ProductDTO> {
    const product = await this.productsRepository.findByName(name);
    if (product === undefined) throw new Error('Product not found');
    const productDTO = ProductMapper.toDTO(product);
    return productDTO;
  }
}
