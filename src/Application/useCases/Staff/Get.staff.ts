import { StaffDto } from '@application/dto/Staff.dto';
import { StaffMapperDto } from '@application/mappers/Staff.mapper';
import { UserNotFound } from '@domain/exceptions/UserNotFound';
import { StaffRepository } from '@domain/repositories/Staff';

export class GetStaffUseCase {
  constructor (private readonly staffRepository: StaffRepository) {}

  async run (dni: string): Promise<StaffDto> {
    const staff = await this.staffRepository.get(dni);
    if (staff === undefined || staff === null) throw new UserNotFound('User not found');
    return StaffMapperDto.domainToDto(staff);
  }
}
