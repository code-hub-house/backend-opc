import { EventDto } from '@application/dto/Event.dto';
import { EventsMapper } from '@application/mappers/Events.mapper';
import { EventsRepository } from '@domain/repositories/Events';

export class GetAllEventsUseCase {
  constructor (
    private readonly eventsRepository: EventsRepository
  ) {}

  async run (): Promise<EventDto[]> {
    const events = await this.eventsRepository.getEvents();
    const eventsDto = EventsMapper.toDTOList(events);
    return eventsDto;
  }
}
