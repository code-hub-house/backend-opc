import { EventDto } from '@application/dto/Event.dto';
import { EventsMapper } from '@application/mappers/Events.mapper';
import { EventsRepository } from '@domain/repositories/Events';

export class UpdateEventUseCase {
  constructor (
    private readonly eventsRepository: EventsRepository
  ) {}

  async run (data: EventDto): Promise<void> {
    const dataEntity = EventsMapper.toEntity(data);
    const event = await this.eventsRepository.getEventById(dataEntity.id);
    if (event === undefined || event === null) throw new Error('Event not found');
    const eventUpdated = await this.eventsRepository.updateEvent(dataEntity);
    if (eventUpdated === undefined || eventUpdated === null) throw new Error('Error updating event');
  }
}
