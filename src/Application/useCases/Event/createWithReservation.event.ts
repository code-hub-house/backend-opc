import { CreateReservationDto, ReservationDto } from '@application/dto/Reservation.dto';
import { ReservationMapper } from '@application/mappers/Reservation.mapper';
import { EventsRepository } from '@domain/repositories/Events';
import { ReservationRepository } from '@domain/repositories/Reservation';
import { StaffRepository } from '@domain/repositories/Staff';
import { CreateEventUseCase } from './create.event';

export class CreateEventWithReservationUseCase {
  constructor (
    private readonly eventsRepository: EventsRepository,
    private readonly reservationRepository: ReservationRepository,
    private readonly staffRepository: StaffRepository
  ) {}

  async run (data: CreateReservationDto): Promise<ReservationDto> {
    const creatorEvent = new CreateEventUseCase(this.eventsRepository, this.staffRepository);
    const event = await creatorEvent.run(data.event);
    const reservation: ReservationDto = {
      cost_center: data.cost_center,
      dependence: data.dependence,
      event,
      overtime: data.overtime,
      space: data.space,
      overtime_description: data.overtime_description,
      status: 'PENDING'
    };
    const reservationEntity = ReservationMapper.toEntity(reservation);
    const reservationCreated = await this.reservationRepository.createReservation(reservationEntity);
    if (reservationCreated === undefined || reservationCreated === null) throw new Error('Error creating reservation');
    return ReservationMapper.toDto(reservationCreated);
  }
}
