import { EventDto } from '@application/dto/Event.dto';
import { EventsMapper } from '@application/mappers/Events.mapper';
import { EventsRepository } from '@domain/repositories/Events';
import { ReservationRepository } from '@domain/repositories/Reservation';

export class DeleteEventUseCase {
  private readonly eventsRepository: EventsRepository;
  private readonly reservationsRepository: ReservationRepository;
  constructor (eventsRepository: EventsRepository, reservationsRepository: ReservationRepository) {
    this.eventsRepository = eventsRepository;
    this.reservationsRepository = reservationsRepository;
  }

  async run (id: string): Promise<EventDto> {
    const event = await this.eventsRepository.getEventById(id);
    if (event === undefined || event === null) throw new Error('Event not found');
    const reservations = await this.reservationsRepository.getReservationByEventId(id);
    reservations?.map(async (reservation) => {
      await this.reservationsRepository.deleteReservation(reservation.id);
    });
    const eventDeleted = await this.eventsRepository.deleteEvent(id);
    if (eventDeleted === undefined || eventDeleted === null) throw new Error('Error deleting event');
    return EventsMapper.toDTO(eventDeleted);
  }
}
