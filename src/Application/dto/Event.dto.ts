export interface EventDto {
  id?: string
  name: string
  description: string
  date: Date
  user_id: string
}
