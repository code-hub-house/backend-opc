import { State } from '@domain/enums/State';
import { EventDto } from './Event.dto';

export interface ReservationDto {
  id?: string
  event: EventDto
  space: string
  cost_center: string
  overtime: boolean
  overtime_description?: string
  dependence: string
  status?: keyof typeof State
}

export interface CreateReservationDto {
  event: EventDto
  space: string
  cost_center: string
  overtime: boolean
  overtime_description?: string
  dependence: string
}
