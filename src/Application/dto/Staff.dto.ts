import { roleCode } from '@domain/enums/Role';

export interface StaffDto {
  readonly dni: string
  name: string
  cellphone: string
  email: string
  password: string
  role: keyof typeof roleCode
  token?: string
}

export interface StaffDtoList {
  users: StaffDto[]
}
