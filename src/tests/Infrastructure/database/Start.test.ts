import { PostgresConnection } from '@infrastructure/driven-adapters/database/connection';

describe('PostgresConnection', () => {
  const postgresConnection = PostgresConnection.getInstance();
  it('should connect to database', async () => {
    await postgresConnection.connect().then(async () => {
      expect(postgresConnection).toBeDefined();
      await postgresConnection.desconnect();
    });
  });

  it('should connect to database with pool', async () => {
    await postgresConnection.query('SELECT NOW()').then((res) => {
      expect(res).toBeDefined();
    });
  });

  it('should connect to database with client', async () => {
    await postgresConnection.connect().then(async () => {
      await postgresConnection.queryClient('SELECT NOW()').then(async (res) => {
        expect(res).toBeDefined();
        await postgresConnection.desconnect();
      });
    });
  });

  it('should connect to database with client and params', async () => {
    await postgresConnection.connect().then(async () => {
      await postgresConnection.queryClient('SELECT $1::text as message', ['Hello world!']).then(async (res) => {
        expect(res).toBeDefined();
        await postgresConnection.desconnect();
      });
    });
  });

  it('should connect to database with pool and params', async () => {
    await postgresConnection.query('SELECT $1::text as message', ['Hello world!']).then((res) => {
      expect(res).toBeDefined();
    });
  });
});
