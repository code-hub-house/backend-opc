import { CreateStaffUseCase } from '@application/useCases/Staff/Create.staff';
import { roleCode } from '@domain/enums/Role';
import { CellphoneInvalid } from '@domain/exceptions/CellphoneInvalid';
import { EmailInvalid } from '@domain/exceptions/EmailInvalid';
import { PasswordInvalid } from '@domain/exceptions/passwordInvalid';
import { RoleCodeError } from '@domain/exceptions/RoleCodeError';
import { StaffAlreadyExist } from '@domain/exceptions/StaffAlreadyExist';

describe('Create staff', () => {
  const staffRepository = {
    save: jest.fn(),
    edit: jest.fn(),
    get: jest.fn(),
    getByEmail: jest.fn(),
    login: jest.fn()
  };

  const staff = {
    dni: '12345678',
    name: 'Staff',
    email: 'staff@gmail.com',
    password: '12345678Aa',
    cellphone: '3001234567',
    role: 'OPCAssistant' as keyof typeof roleCode
  };

  const staffDomain = {
    dni: '12345678',
    name: 'Staff',
    email: 'staff@gmail.com',
    password: '12345678Aa',
    cellphone: '3001234567',
    roleCode: 'OPCAssistant' as keyof typeof roleCode
  };

  it('should create a staff', async () => {
    staffRepository.save.mockResolvedValueOnce(staffDomain);
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    const staffCreated = await createStaffUseCase.run(staff);
    expect(staffCreated).toHaveProperty('dni');
  });

  it('should throw an error if the staff already exists', async () => {
    staffRepository.getByEmail.mockResolvedValueOnce(staff);
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(StaffAlreadyExist);
    });
  });

  it('should throw an error if the staff already exists', async () => {
    staffRepository.get.mockResolvedValueOnce(staff);
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(StaffAlreadyExist);
    });
  });

  it('should throw an error if the staff password is invalid', async () => {
    staff.password = '12345678';
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(PasswordInvalid);
    });
  });

  it('should throw an error if the staff email format is invalid', async () => {
    staff.password = '12345678Aa';
    staff.email = 'staff';
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(EmailInvalid);
    });
  });

  it('should throw an error if the staff cellphone format is invalid', async () => {
    staff.email = 'staff@gmail.com';
    staff.cellphone = '300123456';
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(CellphoneInvalid);
    });
    staff.cellphone = '30012345678';
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(CellphoneInvalid);
    });
  });

  it('should throw an error if the staff role is invalid', async () => {
    staff.cellphone = '3001234567';
    staff.role = 'invalidRole' as keyof typeof roleCode;
    const createStaffUseCase = new CreateStaffUseCase(staffRepository);
    await createStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(RoleCodeError);
    });
  });
});
