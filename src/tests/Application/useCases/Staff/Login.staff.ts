import { StaffDto } from '@application/dto/Staff.dto';
import { StaffMapperDto } from '@application/mappers/Staff.mapper';
import { getStaffInstance } from '@domain/entities/Staff';
import { Unathorized } from '@domain/exceptions/Unauthorized';
import { UserNotFound } from '@domain/exceptions/UserNotFound';
import { StaffRepository } from '@domain/repositories/Staff';

export class LoginStaffUseCase {
  constructor (
    private readonly staffRepository: StaffRepository
  ) {}

  async run (email: string, password: string): Promise<StaffDto> {
    const staff = await this.staffRepository.getByEmail(email);
    if (staff === undefined || staff === null) throw new UserNotFound('User not found');
    const staffInstace = getStaffInstance(staff);
    if (staffInstace === undefined || staffInstace === null) throw new UserNotFound('User not found');
    const isPasswordCorrect = staffInstace.verifyPassword !== undefined ? await staffInstace.verifyPassword(password) : false;
    if (!isPasswordCorrect) throw new Unathorized('Password is incorrect');
    const staffLogged = StaffMapperDto.domainToDto(staffInstace);
    return staffLogged;
  }
}
