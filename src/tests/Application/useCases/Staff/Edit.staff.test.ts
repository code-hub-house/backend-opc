import { EditStaffUseCase } from '@application/useCases/Staff/Edit.staff';
import { roleCode } from '@domain/enums/Role';
import { CellphoneInvalid } from '@domain/exceptions/CellphoneInvalid';
import { EmailInvalid } from '@domain/exceptions/EmailInvalid';
import { PasswordInvalid } from '@domain/exceptions/passwordInvalid';
import { RoleCodeError } from '@domain/exceptions/RoleCodeError';
import { UserNotFound } from '@domain/exceptions/UserNotFound';

describe('Edit staff', () => {
  const staffRepository = {
    get: jest.fn(),
    edit: jest.fn(),
    save: jest.fn(),
    getByEmail: jest.fn(),
    login: jest.fn()
  };

  const staff = {
    dni: '12345678',
    name: 'Staff',
    email: 'staff@hotmail.com',
    password: '12345678Aa',
    cellphone: '3001234567',
    role: 'OPCAssistant' as keyof typeof roleCode
  };

  const staffDomain = {
    dni: '12345678',
    name: 'Staff',
    email: 'staff@hotmail.com',
    password: '12345678Aa',
    cellphone: '3001234567',
    roleCode: 'OPCAssistant' as keyof typeof roleCode
  };

  it('should edit a staff', async () => {
    staffRepository.get.mockResolvedValueOnce(staffDomain);
    staffRepository.edit.mockResolvedValueOnce(staffDomain);
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    const staffEdited = await editStaffUseCase.run(staff);
    expect(staffEdited).toHaveProperty('dni');
  });

  it('should throw an error if the staff does not exist', async () => {
    staffRepository.get.mockResolvedValueOnce(undefined);
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    await editStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(UserNotFound);
    });
  });

  it('should throw an error if the staff email is invalid', async () => {
    staffRepository.get.mockResolvedValueOnce(staffDomain);
    staffRepository.edit.mockResolvedValueOnce(staffDomain);
    staff.email = 'staff';
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    await editStaffUseCase.run(staff).catch((error) => {
      console.log(error);
      expect(error).toBeInstanceOf(EmailInvalid);
    });
  });

  it('should throw an error if the staff password is invalid', async () => {
    staffRepository.get.mockResolvedValueOnce(staffDomain);
    staffRepository.edit.mockResolvedValueOnce(staffDomain);
    staff.email = 'staff@hotmail.com';
    staff.password = '12345678';
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    await editStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(PasswordInvalid);
    });
  });

  it('should throw an error if the staff email already exists', async () => {
    staffRepository.get.mockResolvedValueOnce(staffDomain);
    staffRepository.edit.mockResolvedValueOnce(staffDomain);
    staffRepository.getByEmail.mockResolvedValueOnce(staffDomain);
    staff.password = '12345678AAa';
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    await editStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(EmailInvalid);
    });
  });

  it('should throw an error if the staff cellphone is invalid', async () => {
    staffRepository.get.mockResolvedValueOnce(staffDomain);
    staffRepository.edit.mockResolvedValueOnce(staffDomain);
    staff.cellphone = '300123456';
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    await editStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(CellphoneInvalid);
    });
  });

  it('should throw an error if the staff role is invalid', async () => {
    staffRepository.get.mockResolvedValueOnce(staffDomain);
    staffRepository.edit.mockResolvedValueOnce(staffDomain);
    staff.cellphone = '3001234567';
    staff.role = 'invalidRole' as any;
    const editStaffUseCase = new EditStaffUseCase(staffRepository);
    await editStaffUseCase.run(staff).catch((error) => {
      expect(error).toBeInstanceOf(RoleCodeError);
    });
  });
});
