import { roleCode } from '@domain/enums/Role';
import { CellphoneInvalid } from '@domain/exceptions/CellphoneInvalid';
import { EmailInvalid } from '@domain/exceptions/EmailInvalid';
import { PasswordInvalid } from '@domain/exceptions/passwordInvalid';
import { RoleCodeError } from '@domain/exceptions/RoleCodeError';
import { StaffAlreadyExist } from '@domain/exceptions/StaffAlreadyExist';
import { StaffProps } from '@domain/interfaces/StaffProps';
import { StaffValidations } from '@domain/services/StaffValidations';

describe('StaffValidations', () => {
  const staffRepository = {
    save: jest.fn(),
    edit: jest.fn(),
    get: jest.fn(),
    getByEmail: jest.fn(),
    login: jest.fn()
  };

  const staff: StaffProps = {
    dni: '12345678A',
    name: 'John',
    cellphone: '666666666',
    email: 'jhon',
    password: '12345678A',
    roleCode: 'OPCAssistant' as keyof typeof roleCode
  };

  const staffValidations = new StaffValidations(staffRepository);

  describe('runValidationsToCreate', () => {
    it("Should throw an exception if the staff's dni is invalid", async () => {
      staffRepository.get.mockResolvedValueOnce(staff);
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(StaffAlreadyExist);
    });

    it("Should throw an exception if the staff's email is invalid", async () => {
      staffRepository.getByEmail.mockResolvedValueOnce(staff);
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(StaffAlreadyExist);
    });

    it("Should throw an exception if the staff's password is invalid (Password must be at least 8 characters long)", async () => {
      staff.password = '1234567';
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(PasswordInvalid);
    });

    it("Should throw an exception if the staff's password is invalid (Password must contain at least one uppercase letter)", async () => {
      staff.password = '12345678';
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(PasswordInvalid);
    });

    it("Should throw an exception if the staff's password is invalid (Password must contain at least one lowercase letter)", async () => {
      staff.password = '12345678A';
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(PasswordInvalid);
    });

    it("Should throw an exception if the staff's password is invalid (Password must contain at least one number)", async () => {
      staff.password = 'abcdefghdasdASADS';
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(PasswordInvalid);
    });

    it("Should throw an exception if the staff's email format is invalid", async () => {
      staff.password = '12345678aA';
      staff.email = 'jhon';
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(EmailInvalid);
    });

    it('Should return an error if the cellphone is invalid', async () => {
      staff.email = 'jhondue@hotmail.com';
      staff.cellphone = '3001234';
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(CellphoneInvalid);
    });

    it("Should throw an exception if the staff's role is invalid", async () => {
      staff.cellphone = '3001234567';
      staff.roleCode = 'Student' as keyof typeof roleCode;
      await expect(
        staffValidations.runValidationsToCreate(staff)
      ).rejects.toThrowError(RoleCodeError);
    });

    it('Should return void if the staff is valid', async () => {
      staff.roleCode = 'OPCAssistant' as keyof typeof roleCode;
      await expect(staffValidations.runValidationsToCreate(staff)).resolves.toBeUndefined();
    });
  });
});
