import { getStaffInstance } from '@domain/entities/Staff';
import { Staff } from '@domain/entities/Staff/Staff';
import { StaffAdministrative } from '@domain/entities/Staff/StaffAdministrative';
import { StaffOPC } from '@domain/entities/Staff/StaffOPC';

describe('Staff', () => {
  const staff = getStaffInstance({
    dni: '12345678',
    name: 'John',
    email: 'jhondue@hotmail.com',
    password: '12345678A',
    cellphone: '3001234567',
    roleCode: 'ADMINTeacher'
  }) as Staff;

  it('should be able to create a new Staff with OPCAssistant role', () => {
    staff.roleCode = 'OPCAssistant';
    expect(getStaffInstance(staff)).toBeInstanceOf(StaffOPC);
  });

  it('should be able to create a new Staff with OPCCoordinator role', () => {
    staff.roleCode = 'OPCCoordinator';
    expect(getStaffInstance(staff)).toBeInstanceOf(StaffOPC);
  });

  it('should be able to create a new Staff with ADMINExternal role', () => {
    staff.roleCode = 'ADMINExternal';
    expect(getStaffInstance(staff)).toBeInstanceOf(StaffAdministrative);
  });

  it('should be able to create a new Staff with ADMINTeacher role', () => {
    staff.roleCode = 'ADMINTeacher';
    expect(getStaffInstance(staff)).toBeInstanceOf(StaffAdministrative);
  });

  it('should return undefined if the role is invalid', () => {
    staff.roleCode = 'invalidRole' as any;
    expect(getStaffInstance(staff)).toBeUndefined();
  });
});
