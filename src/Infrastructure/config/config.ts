import dotenv from 'dotenv';
import path from 'path';

dotenv.config({
  path: path.resolve(
    __dirname, `../../../${process.env.NODE_ENV ?? 'development'}.env`
  )
});

const config = {

  DATABASE: {
    user: process.env.DATABASE_USER ?? 'postgres',
    host: process.env.DATABASE_HOST,
    port: (process.env.DATABASE_PORT !== undefined) ? parseInt(process.env.DATABASE_PORT) : 5432,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
  },
  NODE_ENV: process.env.NODE_ENV,
  PORT: process.env.PORT,
  SECRET_KEY: process.env.SECRET_KEY
};
console.log(config.DATABASE.password);

export default config;
