/* eslint-disable @typescript-eslint/no-misused-promises */
import { Router } from 'express';
import { createUserController } from '../controllers/staff/createStaff';
import { getUserByDNIController } from '../controllers/staff/getStaffByDNI';
import { updateUserController } from '../controllers/staff/updateStaff';
import { verifyRoleSuperAdmin } from '../middlewares/verifyRole';

const routerStaff = Router();

routerStaff.get('/', getUserByDNIController);

routerStaff.post('/', verifyRoleSuperAdmin, createUserController);

routerStaff.put('/', updateUserController);

export default routerStaff;
