import { Router } from 'express';

const productRouter = Router();

productRouter.get('/', (req, res) => {
  res.json({ message: 'get all products' });
});

productRouter.get('/:id', (req, res) => {
  res.json({ message: 'get product by id' });
});

productRouter.post('/', (req, res) => {
  res.json({ message: 'create product' });
});

productRouter.put('/:id', (req, res) => {
  res.json({ message: 'update product' });
});

productRouter.delete('/:id', (req, res) => {
  res.json({ message: 'delete product' });
});

export default productRouter;
