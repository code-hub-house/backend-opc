import { Router } from 'express';
import { getEventByDNI } from '../controllers/event/getById';

const eventRouter = Router();

eventRouter.get('/', (req, res, next) => {
  const name = req.query.name;
  const userId = req.query.user_id;
  if (name !== undefined) {
    res.json({ message: 'get all events by name' });
  } else if (userId !== undefined) {
    void getEventByDNI(req, res, next);
  } else {
    res.json({ message: 'get all events' });
  }
});

eventRouter.get('/:id', (req, res) => {
  res.json({ message: 'get event by id' });
});

eventRouter.post('/', (req, res) => {
  const withReservation = req.query.withReservation;
  if (withReservation !== undefined) {
    res.json({ message: 'create event with reservation' });
  } else {
    res.json({ message: 'create event' });
  }
});

eventRouter.put('/:id', (req, res) => {
  res.json('update event');
});

eventRouter.delete('/:id', (req, res) => {
  res.json('delete event');
});

export default eventRouter;
