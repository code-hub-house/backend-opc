import { Router } from 'express';

const spaceRouter = Router();

spaceRouter.get('/', (req, res) => {
  res.json({ message: 'get all spaces' });
});

spaceRouter.get('/:id', (req, res) => {
  res.json({ message: 'get space by id' });
});

spaceRouter.post('/', (req, res) => {
  res.json({ message: 'create space' });
});

spaceRouter.put('/:id', (req, res) => {
  res.json({ message: 'update space' });
});

spaceRouter.delete('/:id', (req, res) => {
  res.json({ message: 'delete space' });
});

export default spaceRouter;
