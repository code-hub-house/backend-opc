import { Response, Request, NextFunction } from 'express';
import { UpdateEventUseCase } from '@application/useCases/Event/update.event';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';

export const updateEvent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const eventRepository = new PostgresEventsRepository();
    const eventUseCase = new UpdateEventUseCase(eventRepository);
    const result = await eventUseCase.run(req.body);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
