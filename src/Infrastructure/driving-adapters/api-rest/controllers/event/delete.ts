import { Response, Request, NextFunction } from 'express';
import { DeleteEventUseCase } from '@application/useCases/Event/delete.event';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';

export const deleteEvent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const eventRepository = new PostgresEventsRepository();
    const reservationRepository = new PostgresReservationRepository();
    const eventUseCase = new DeleteEventUseCase(eventRepository, reservationRepository);
    const result = await eventUseCase.run(req.params.id);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
