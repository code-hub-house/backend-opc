import { Response, Request, NextFunction } from 'express';
import { GetAllEventsUseCase } from '@application/useCases/Event/getAll.event';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';

export const getAllEvents = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const eventRepository = new PostgresEventsRepository();
    const eventUseCase = new GetAllEventsUseCase(eventRepository);
    const result = await eventUseCase.run();
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
