import { Response, Request, NextFunction } from 'express';
import { GetByEventIdUseCase } from '@application/useCases/Event/getById.event';
import { PostgresEventsRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Events.repository';
import { GetByEventDNIUseCase } from '@application/useCases/Event/getByDni.event';

export const getEventById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const eventRepository = new PostgresEventsRepository();
    const eventUseCase = new GetByEventIdUseCase(eventRepository);
    const result = await eventUseCase.run(req.params.id);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export const getEventByDNI = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const eventRepository = new PostgresEventsRepository();
    const eventUseCase = new GetByEventDNIUseCase(eventRepository);
    const result = await eventUseCase.run(req.query.user_id as string);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
