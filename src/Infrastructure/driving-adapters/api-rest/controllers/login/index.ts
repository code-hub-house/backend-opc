import { LoginDto } from '@application/dto/login.dto';
import { Auth } from '@application/useCases/auth';
import { PostgresStaffRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Staff.repository';
import { JWT } from '@infrastructure/driven-adapters/jwt';
import { Bcrypt } from '@infrastructure/driven-adapters/jwt/Bcrypt';
import { NextFunction, Request, Response } from 'express';

export const login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const { email, password } = req.body as LoginDto;
    const postgresStaffRepository = new PostgresStaffRepository();
    const jwt = new JWT();
    const bcrypt = new Bcrypt(10);
    const auth = new Auth(jwt, bcrypt, postgresStaffRepository);
    const token = await auth.login({ email, password });
    if (token !== null) {
      res.status(200).json(token);
    } else {
      res.status(404).json({ message: 'Staff not found' });
    }
  } catch (error) {
    next(error);
  }
};

export const loginController = (req: Request, res: Response, next: NextFunction): void => {
  login(req, res, next).then().catch(next);
};
