import { SetStateReservationUseCase } from '@application/useCases/Reservation/setState.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const setStateReservation = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new SetStateReservationUseCase(reservationRepository);
  const result = await reservationUseCase.run(req.params.id, req.body.state);
  res.status(200).json(result);
};
