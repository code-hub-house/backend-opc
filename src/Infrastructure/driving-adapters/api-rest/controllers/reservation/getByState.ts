import { GetReservationByStateUseCase } from '@application/useCases/Reservation/getByState.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const getReservationByState = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new GetReservationByStateUseCase(reservationRepository);
  const result = await reservationUseCase.run(req.params.id);
  res.status(200).json(result);
};
