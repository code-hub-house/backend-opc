import { GetReservationByUserDniUseCase } from '@application/useCases/Reservation/getByUserdni.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const getReservationByUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new GetReservationByUserDniUseCase(reservationRepository);
  const result = await reservationUseCase.run(req.query.user_id as string);
  res.status(200).json(result);
};
