import { UpdateReservationUseCase } from '@application/useCases/Reservation/update.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';
import { NextFunction, Request, Response } from 'express';

export const updateReservation = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new UpdateReservationUseCase(reservationRepository);
  const result = await reservationUseCase.run(req.body);
  res.status(200).json(result);
};
