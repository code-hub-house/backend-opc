import { NextFunction, Request, Response } from 'express';
import { addProductController } from './addProduct';
import { createReservationController } from './createReservation';
import { deleteReservation } from './deleteReservation';
import { getAllReservationsController } from './getAllReservations';
import { getReservationByIdController } from './getById';
import { getReservationByState } from './getByState';
import { getReservationByUser } from './getByUser';
import { setStateReservation } from './setState';
import { updateReservation } from './updateReservation';

export class ReservationControllers {
  getAll = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await getAllReservationsController(req, res, next).then().catch(next);
  };

  create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await createReservationController(req, res, next).then().catch(next);
  };

  update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await updateReservation(req, res, next).then().catch(next);
  };

  delete = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await deleteReservation(req, res, next).then().catch(next);
  };

  get = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await getReservationByIdController(req, res, next).then().catch(next);
  };

  getByEvent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    // getReservationByEvent(req, res, next).then().catch(next);
  };

  getByUser = (req: Request, res: Response, next: NextFunction): void => {
    getReservationByUser(req, res, next).then().catch(next);
  };

  getByState = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await getReservationByState(req, res, next).then().catch(next);
  };

  setState = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await setStateReservation(req, res, next).then().catch(next);
  };

  addProduct = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await addProductController(req, res, next).then().catch(next);
  };
}
