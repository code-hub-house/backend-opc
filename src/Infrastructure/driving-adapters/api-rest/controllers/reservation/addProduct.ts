import { Response, Request, NextFunction } from 'express';
import { AddProductToReservationUseCase } from '@application/useCases/Reservation/addProduct.reservation';
import { PostgresReservationRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Reservation.repository';

export const addProductController = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const reservationRepository = new PostgresReservationRepository();
  const reservationUseCase = new AddProductToReservationUseCase(reservationRepository);
  const result = await reservationUseCase.run(req.params.id, req.body);
  res.status(200).json(result);
};
