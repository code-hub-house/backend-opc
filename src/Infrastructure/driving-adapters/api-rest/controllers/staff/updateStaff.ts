import { StaffDto } from '@application/dto/Staff.dto';
import { EditStaffUseCase } from '@application/useCases/Staff/Edit.staff';
import { PostgresStaffRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Staff.repository';
import { NextFunction, Request, Response } from 'express';

const updateStaff = () => async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const staff = req.body as StaffDto;
  const postgresStaffRepository = new PostgresStaffRepository();
  const staffUpdateUseCase = new EditStaffUseCase(postgresStaffRepository);
  const staffUpdated = await staffUpdateUseCase.run(staff);
  if (staffUpdated !== null) {
    res.status(200).json(staffUpdated);
  }
  res.status(404).json({ message: 'Staff not found' });
};

export const updateUserController = (req: Request, res: Response, next: NextFunction): void => {
  updateStaff()(req, res, next).catch(next);
};
