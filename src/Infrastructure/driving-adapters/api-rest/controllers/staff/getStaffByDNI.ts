import { GetStaffUseCase } from '@application/useCases/Staff/Get.staff';
import { PostgresStaffRepository } from '@infrastructure/driven-adapters/database/postgres/implementation/Staff.repository';
import { Request, Response, NextFunction } from 'express';

const getStaffByDNI = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const { dni } = req.params;
  const postgresStaffRepository = new PostgresStaffRepository();
  const getStaffUseCase = new GetStaffUseCase(postgresStaffRepository);
  const staff = await getStaffUseCase.run(dni);
  if (staff !== null) {
    res.status(200).json(staff);
  }
  res.status(404).json({ message: 'Staff not found' });
};

export const getUserByDNIController = (req: Request, res: Response, next: NextFunction): void => {
  getStaffByDNI(req, res, next).catch(next);
};
