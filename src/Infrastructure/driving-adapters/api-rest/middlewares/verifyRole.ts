import { JWT } from '@infrastructure/driven-adapters/jwt';
import { NextFunction, Request, Response } from 'express';

export const verifyRoleAdminExternal = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const authorization = req.headers.authorization;
  const token = authorization?.split(' ')[1];
  if (token !== undefined) {
    const jwt = new JWT();
    const decoded = await jwt.verify(token);
    if (decoded?.role === 'admin-external') {
      next();
    } else {
      res.status(403).send('Forbidden');
    }
  } else res.status(403).send('No token provided');
};

export const verifyRoleAdminTeacher = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const authorization = req.headers.authorization;
  const token = authorization?.split(' ')[1];
  if (token !== undefined) {
    const jwt = new JWT();
    const decoded = await jwt.verify(token);
    if (decoded?.role === 'admin-proffesor') {
      next();
    } else {
      res.status(403).send('Forbidden');
    }
  } else res.status(403).send('No token provided');
};

export const verifyRoleAdmin = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  await verifyRoleAdminExternal(req, res, next);
  await verifyRoleAdminTeacher(req, res, next);
};

export const verifyRoleOPCAssitant = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const authorization = req.headers.authorization;
  const token = authorization?.split(' ')[1];
  if (token !== undefined) {
    const jwt = new JWT();
    const decoded = await jwt.verify(token);
    if (decoded?.role === 'opc-assistant') {
      next();
    } else {
      res.status(403).send('Forbidden');
    }
  } else res.status(403).send('No token provided');
};

export const verifyRoleOPCCoordinator = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const authorization = req.headers.authorization;
  const token = authorization?.split(' ')[1];
  if (token !== undefined) {
    const jwt = new JWT();
    const decoded = await jwt.verify(token);
    if (decoded?.role === 'opc-coordinator') {
      next();
    } else {
      res.status(403).send('Forbidden');
    }
  } else res.status(403).send('No token provided');
};

export const verifyRoleOPC = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  await verifyRoleOPCAssitant(req, res, next);
  await verifyRoleOPCCoordinator(req, res, next);
};

export const verifyRole = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  await verifyRoleAdmin(req, res, next);
  await verifyRoleOPC(req, res, next);
};

export const verifyRoleSuperAdmin = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const authorization = req.headers.authorization;
  const token = authorization?.split(' ')[1];
  if (token !== undefined) {
    const jwt = new JWT();
    const decoded = await jwt.verify(token);
    if (decoded?.role === 'SUPERADMIN') {
      next();
    } else {
      res.status(403).json({ message: 'Forbidden' });
    }
  } else res.status(403).json({ message: 'No token provided' });
};
