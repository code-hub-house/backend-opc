import { JWT } from '@infrastructure/driven-adapters/jwt';
import { NextFunction, Request, Response } from 'express';

const verifyToken = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const bearerHeader = req.headers.authorization;
  if (typeof bearerHeader !== 'undefined') {
    console.log(bearerHeader);
    const bearer = bearerHeader.split(' ');
    console.log(bearer);
    const bearerToken = bearer[1];
    const jwt = new JWT();
    const decoded = await jwt.verify(bearerToken);
    if (decoded !== null) {
      console.log('user decoded: ', decoded);
      next();
    } else {
      res.status(403).json({ message: 'Invalid token' });
    }
  } else {
    res.status(403).json({ message: 'Invalid token' });
  }
};

export const verifyTokenController = (req: Request, res: Response, next: NextFunction): void => {
  verifyToken(req, res, next).catch(next);
};
