import { PostgresConnection } from '@infrastructure/driven-adapters/database/connection';
import { OPCApp } from './OPCApp';

try {
  PostgresConnection.getInstance();
  void new OPCApp().start();
} catch (error) {
  console.log(error);
}
