### Capa de infraestructura de la aplicación
Esta capa contiene los adaptadores de la aplicación. Los adaptadores son los que se encargan de la comunicación entre las capas de la aplicación y el exterior.

#### Adaptadores
Los adaptadores son los que se encargan de la comunicación entre las capas de la aplicación y el exterior.

##### Adaptador: Base de datos
Este adaptador se encarga de la comunicación con la base de datos.

##### Adaptador: Servidor web
Este adaptador se encarga de la comunicación con el exterior.

##### Adaptador: Servicio de autenticación
Este adaptador se encarga de la comunicación con el servicio de autenticación.
