// import { LoggerService } from '@infrastructure/driven-adapters/logger';
import config from '@infrastructure/config/config';
import { LoggerService } from '@infrastructure/driven-adapters/logger';
import { Client, Pool } from 'pg';
import { client, pool } from './connection';

export class PostgresConnection {
  private static instance: PostgresConnection;
  private client: Client | undefined;
  private readonly pool: Pool | undefined;

  private constructor () {
    const logger = new LoggerService();
    this.client = client;
    this.pool = pool;
    void this.connectPool().then(() => logger.log('Connected to database'));
  }

  public static getInstance (): PostgresConnection {
    if (PostgresConnection.instance === undefined || PostgresConnection.instance === null) {
      PostgresConnection.instance = new PostgresConnection();
    }
    return PostgresConnection.instance;
  }

  private async connectPool (): Promise<void> {
    await this.pool?.connect();
  }

  public async connect (): Promise<void> {
    this.client = await new Client(config.DATABASE);
    await this.client.connect();
  }

  public async desconnect (): Promise<void> {
    await this.client?.end();
  }

  public async queryClient (query: string, params?: any[]): Promise<any> {
    if (params === undefined || params === null) {
      return await this.client?.query(query);
    }
    return await this.client?.query(query, params);
  }

  public async query (query: string, params?: any[]): Promise<any> {
    if (params === undefined || params === null) {
      return await this.pool?.query(query);
    }
    return await this.pool?.query(query, params);
  }
}
