import config from '@infrastructure/config/config';
import { Client, Pool } from 'pg';

const pool = new Pool({
  connectionString: `postgresql://${config.DATABASE.user}:${config.DATABASE.password as string}@${config.DATABASE.host as string}:${config.DATABASE.port}/${config.DATABASE.database as string}`
});

const client = new Client({
  connectionString: `postgresql://${config.DATABASE.user}:${config.DATABASE.password as string}@${config.DATABASE.host as string}:${config.DATABASE.port}/${config.DATABASE.database as string}`
});

export { client, pool };
