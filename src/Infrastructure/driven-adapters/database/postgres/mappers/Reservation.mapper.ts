import { Reservation } from '@domain/entities/reservation/reservation';
import { ReservationDao } from '../dao/Reservation.dao';

export const ReservationMapper = {
  toEntity: (dao: ReservationDao): Reservation => {
    return new Reservation({
      id: dao.created_at.toString(),
      event_id: dao.id_event,
      space_id: dao.id_space,
      ticket: {
        id: dao.created_at.toString(),
        cost_center: dao.cost_center,
        dependence: dao.dependence,
        overtime: dao.overtime,
        overtime_description: dao.overtime_description,
        state: dao.state
      }
    });
  },
  toDao: (reservation: Reservation): ReservationDao => {
    return {
      created_at: new Date(reservation.id),
      id_event: reservation.event_id,
      id_space: reservation.space_id,
      cost_center: reservation.ticket.cost_center,
      dependence: reservation.ticket.dependence,
      overtime: reservation.ticket.overtime,
      overtime_description: reservation.ticket.overtime_description ?? '',
      state: reservation.ticket.state
    };
  },
  toEntityList: (daos: ReservationDao[]): Reservation[] => {
    return daos.map((dao) => ReservationMapper.toEntity(dao));
  },
  toDaoList: (reservations: Reservation[]): ReservationDao[] => {
    return reservations.map((reservation) => ReservationMapper.toDao(reservation));
  }
};
