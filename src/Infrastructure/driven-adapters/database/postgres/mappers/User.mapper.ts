import { getStaffInstance } from '@domain/entities/Staff';
import { Staff } from '@domain/entities/Staff/Staff';
import { StaffProps } from '@domain/interfaces/StaffProps';
import { UserDao } from '../dao/User.dao';

export const UserMapper = {
  toDomain (userDao: UserDao): Staff | undefined {
    const staff: StaffProps = {
      dni: userDao.dni,
      name: userDao.name,
      email: userDao.email,
      cellphone: userDao.cellphone,
      password: userDao.password,
      roleCode: userDao.role
    };
    return getStaffInstance(staff);
  },
  toDomainList (userDaoList: UserDao[]): Staff[] {
    return userDaoList.map(userDao => UserMapper.toDomain(userDao) as Staff);
  },
  toPersistence (staff: Staff | StaffProps): UserDao {
    return {
      dni: staff.dni,
      name: staff.name,
      email: staff.email,
      cellphone: staff.cellphone,
      password: staff.password,
      role: staff.roleCode
    };
  },
  toPersistenceList (staffList: Staff[]): UserDao[] {
    return staffList.map(staff => this.toPersistence(staff));
  }
};
