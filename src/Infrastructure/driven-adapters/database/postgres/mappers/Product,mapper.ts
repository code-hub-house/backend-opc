import { Product } from '@domain/entities/products/products';
import { ProductDao } from '../dao/Product.dao';

export const ProductMapper = {
  toDomain: (productDao: ProductDao): Product => ({
    id: productDao.id,
    name: productDao.name
  }),
  toDao: (product: Product): ProductDao => ({
    id: product.id,
    name: product.name
  }),
  toDomainList: (productsDao: ProductDao[]): Product[] => {
    return productsDao.map((productDao) => ProductMapper.toDomain(productDao));
  },
  toDaoList: (products: Product[]): ProductDao[] => {
    return products.map((product) => ProductMapper.toDao(product));
  }
};
