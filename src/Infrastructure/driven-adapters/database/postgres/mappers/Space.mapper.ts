import { Space } from '@domain/entities/space/Space';
import { SpaceDao } from '../dao/Space.dao';

export const SpaceMapper = {
  toDomain: (dao: SpaceDao): Space => {
    const number = parseInt(dao.space_number);
    return {
      id: dao.id,
      name: dao.name,
      number,
      state: 'active',
      capacity: dao.capacity
    };
  },
  toDao: (space: Space): SpaceDao => {
    return {
      id: space.id,
      name: space.name,
      space_number: space.number.toString(),
      capacity: space.capacity
    };
  },
  toDomainList: (daos: SpaceDao[]): Space[] => {
    return daos.map((dao) => SpaceMapper.toDomain(dao));
  },
  toDaoList: (spaces: Space[]): SpaceDao[] => {
    return spaces.map((space) => SpaceMapper.toDao(space));
  }
};
