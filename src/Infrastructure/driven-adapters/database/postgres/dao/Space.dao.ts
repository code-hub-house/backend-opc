export interface SpaceDao {
  id: string
  name: string
  space_number: string
  capacity: number
}
