export interface EventDao {
  id: string
  dni_user: string
  name: string
  description: string
  date: Date
}
