import { State } from '@domain/enums/State';

export interface ReservationDao {
  created_at: Date
  id_event: string
  id_space: string
  cost_center: string
  overtime: boolean
  overtime_description: string
  dependence: string
  state: keyof typeof State
}
