import { roleCode } from '@domain/enums/Role';

export interface UserDao {
  dni: string
  name: string
  cellphone: string
  email: string
  password: string
  role: keyof typeof roleCode
}
