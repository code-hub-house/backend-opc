import { StaffProps } from '@domain/interfaces/StaffProps';
import { StaffRepository } from '@domain/repositories/Staff';
import { PostgresConnection } from '../../connection';
import { UserMapper } from '../mappers/User.mapper';

export class PostgresStaffRepository implements StaffRepository {
  private readonly connection: PostgresConnection;

  constructor () {
    this.connection = PostgresConnection.getInstance();
  }

  public async save (staff: StaffProps): Promise<StaffProps> {
    const staffDao = UserMapper.toPersistence(staff);
    const query = 'INSERT INTO public."Users" (dni, name, email, cellphone, password, role) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *';
    const values = [
      staffDao.dni,
      staffDao.name,
      staffDao.email,
      staffDao.cellphone,
      staffDao.password,
      staffDao.role
    ];
    const result = await this.connection.query(query, values);
    return UserMapper.toDomain(result.rows[0]) as StaffProps;
  }

  public async edit (staff: StaffProps): Promise<StaffProps> {
    const staffDao = UserMapper.toPersistence(staff);
    const query = 'UPDATE public."Users" SET name = $1, email = $2, cellphone = $3, password = $4, role = $5 WHERE dni = $6 RETURNING *';
    const values = [
      staffDao.name,
      staffDao.email,
      staffDao.cellphone,
      staffDao.password,
      staffDao.role,
      staffDao.dni
    ];
    const result = await this.connection.query(query, values);
    return UserMapper.toDomain(result.rows[0]) as StaffProps;
  }

  public async get (dni: string): Promise<StaffProps | undefined> {
    const query = 'SELECT * FROM public."Users" WHERE dni = $1';
    const result = await this.connection.query(query, [dni]);
    return result.rows.length > 0 ? UserMapper.toDomain(result.rows[0]) : undefined;
  }

  public async getByEmail (email: string): Promise<StaffProps | undefined> {
    const query = 'SELECT * FROM  public."Users" WHERE email = $1';
    const result = await this.connection.query(query, [email]);
    if (result.rows.length === 0) return undefined;
    return UserMapper.toDomain(result.rows[0]);
  }

  public async login (
    email: string,
    password: string
  ): Promise<StaffProps | undefined> {
    const query = 'SELECT * FROM public."Users" WHERE email = $1 AND password = $2';
    const result = await this.connection.query(query, [email, password]);
    return UserMapper.toDomain(result.rows[0]);
  }
}
