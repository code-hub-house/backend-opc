import { Space } from '@domain/entities/space/Space';
import { SpaceRepository } from '@domain/repositories/Spaces';
import { PostgresConnection } from '../../connection';
import { SpaceMapper } from '../mappers/Space.mapper';

export class PostgresSpaceRepository implements SpaceRepository {
  private readonly connection: PostgresConnection;

  constructor () {
    this.connection = PostgresConnection.getInstance();
  }

  async create (data: Space): Promise<Space> {
    const query = 'INSERT INTO spaces (name) VALUES ($1) RETURNING *';
    const values = [data.name];
    const result = await this.connection.query(query, values);
    return SpaceMapper.toDomain(result.rows[0]);
  }

  async update (data: Space): Promise<Space> {
    const query = 'UPDATE spaces SET name = $1 WHERE id = $2 RETURNING *';
    const values = [data.name, data.id];
    const result = await this.connection.query(query, values);
    return SpaceMapper.toDomain(result.rows[0]);
  }

  async delete (id: string): Promise<Space> {
    const query = 'DELETE FROM spaces WHERE id = $1 RETURNING *';
    const result = await this.connection.query(query, [id]);
    return SpaceMapper.toDomain(result.rows[0]);
  }

  async findByName (name: string): Promise<Space | undefined> {
    const query = 'SELECT * FROM spaces WHERE name = $1';
    const result = await this.connection.query(query, [name]);
    return SpaceMapper.toDomain(result.rows[0]);
  }

  async findById (id: string): Promise<Space | undefined> {
    const query = 'SELECT * FROM spaces WHERE id = $1';
    const result = await this.connection.query(query, [id]);
    return SpaceMapper.toDomain(result.rows[0]);
  }

  async findAll (): Promise<Space[]> {
    const query = 'SELECT * FROM spaces';
    const result = await this.connection.query(query);
    return result.rows.map(SpaceMapper.toDomain);
  }
}
