import { Event } from '@domain/entities/events/events';
import { EventsRepository } from '@domain/repositories/Events';
import { PostgresConnection } from '../../connection';
import { EventMapper } from '../mappers/Event.mapper';

export class PostgresEventsRepository implements EventsRepository {
  private readonly connection: PostgresConnection;

  constructor () {
    this.connection = PostgresConnection.getInstance();
  }

  async createEvent (event: Event): Promise<Event> {
    const query = 'INSERT INTO public."Events" (id, dni_user, name, date, description) VALUES ($1, $2, $3, $4, $5) RETURNING *';
    const values = [
      event.id,
      event.staff_id,
      event.name,
      event.date,
      event.description
    ];
    const result = await this.connection.query(query, values);
    const eventCreated = EventMapper.toDomain(result.rows[0]);
    return eventCreated;
  }

  async updateEvent (event: Event): Promise<Event> {
    const query = 'UPDATE public."Events" SET dni_user = $1, name = $2, date = $3, description = $4 WHERE id = $5 RETURNING *';
    const values = [
      event.staff_id,
      event.name,
      event.date,
      event.description,
      event.id
    ];
    const result = await this.connection.query(query, values);
    const eventUpdated = EventMapper.toDomain(result.rows[0]);
    return eventUpdated;
  }

  async deleteEvent (id: string): Promise<Event> {
    const query = 'DELETE FROM public."Events" WHERE id = $1 RETURNING *';
    const result = await this.connection.query(query, [id]);
    const eventDeleted = EventMapper.toDomain(result.rows[0]);
    return eventDeleted;
  }

  async findEventByName (name: string): Promise<Event | undefined> {
    const query = 'SELECT * FROM public."Events" WHERE name = $1';
    const result = await this.connection.query(query, [name]);
    const eventFound = EventMapper.toDomain(result.rows[0]);
    return eventFound;
  }

  async getEventById (id: string): Promise<Event | undefined> {
    const query = 'SELECT * FROM public."Events" WHERE id = $1';
    const result = await this.connection.query(query, [id]);
    const eventFound = EventMapper.toDomain(result.rows[0]);
    return eventFound;
  }

  async getEvents (): Promise<Event[]> {
    const query = 'SELECT * FROM public."Events"';
    const result = await this.connection.query(query);
    const eventsFound = result.rows.map(EventMapper.toDomain);
    return eventsFound;
  }

  async getEventsByDni (dni: string): Promise<Event[]> {
    const query = 'SELECT * FROM public."Events" WHERE dni_user = $1';
    const result = await this.connection.query(query, [dni]);
    const eventsFound = result.rows.map(EventMapper.toDomain);
    return eventsFound;
  }
}
