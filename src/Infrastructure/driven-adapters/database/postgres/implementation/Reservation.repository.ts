import { ReservationProps } from '@domain/interfaces/ReservationProps';
import { ReservationRepository } from '@domain/repositories/Reservation';
import { PostgresConnection } from '../../connection';
import { ReservationMapper } from '../mappers/Reservation.mapper';

export class PostgresReservationRepository implements ReservationRepository {
  private readonly connection: PostgresConnection;

  constructor () {
    this.connection = PostgresConnection.getInstance();
  }

  async createReservation (reservation: ReservationProps): Promise<ReservationProps> {
    const ReservationDao = ReservationMapper.toDao(reservation);
    const query = 'INSERT INTO public."Reservations" (created_at, id_event, id_space, cost_center, overtime, overtime_description, dependence, state) VALUES ($1, $2, $3, $4, $5, $6, $7, "pending") RETURNING *';
    const values = [
      ReservationDao.created_at,
      ReservationDao.id_event,
      ReservationDao.id_space,
      ReservationDao.cost_center,
      ReservationDao.overtime,
      ReservationDao.overtime_description,
      ReservationDao.dependence
    ];
    const result = await this.connection.query(query, values);
    return ReservationMapper.toEntity(result.rows[0]) as ReservationProps;
  }

  async getReservationById (id: string): Promise<ReservationProps | undefined> {
    const query = 'SELECT * FROM public."Reservations" WHERE id = $1';
    const result = await this.connection.query(query, [id]);
    return ReservationMapper.toEntity(result.rows[0]);
  }

  async getReservations (): Promise<ReservationProps[]> {
    const query = 'SELECT * FROM public."Reservations"';
    const result = await this.connection.query(query);
    return result.rows.map(ReservationMapper.toEntity);
  }

  async getReservationByState (state: string): Promise<ReservationProps[]> {
    const query = 'SELECT * FROM public."Reservations" WHERE state = $1';
    const result = await this.connection.query(query, [state]);
    return result.rows.map(ReservationMapper.toEntity);
  }

  async getReservationBySpaceId (spaceId: string): Promise<ReservationProps[]> {
    const query = 'SELECT * FROM public."Reservations" WHERE id_space = $1';
    const result = await this.connection.query(query, [spaceId]);
    return result.rows.map(ReservationMapper.toEntity);
  }

  async getReservationByEventId (eventId: string): Promise<ReservationProps[]> {
    const query = 'SELECT spacem.name as space_name,eventsm.name as eventName, created_at, id_event, id_space, cost_center, overtime, overtime_description, dependence, state, dni_user, userm.name as username, date, description as event_description, space_number, capacity FROM public."Reservations" inner join public."Events" as eventsm on  id = id_event inner join public."Spaces" as spacem on id_space = spacem.id  inner join public."Users" as userm on dni_user = userm.dni WHERE id_event = $1';

    const result = await this.connection.query(query, [eventId]);
    console.log(result.rows);
    const reservations = ReservationMapper.toEntityList(result.rows);
    return reservations;
  }

  async getReservationsByUser (userId: string): Promise<ReservationProps[]> {
    console.log(userId);
    const query = 'SELECT spacem.name as space_name,eventsm.name as eventName, created_at, id_event, id_space, cost_center, overtime, overtime_description, dependence, state, dni_user, userm.name as username, date, description as event_description, space_number, capacity FROM public."Reservations" inner join public."Events" as eventsm on  id = id_event inner join public."Spaces" as spacem on id_space = spacem.id  inner join public."Users" as userm on dni_user = userm.dni WHERE dni_user = $1';
    const result = await this.connection.query(query, [userId]);
    const reservations = ReservationMapper.toEntityList(result.rows);
    const reservationsWithSpace = result.rows.map((reservation: any, index: any) => {
      return {
        ...reservations[index],
        space: {
          name: reservation.space_name
        },
        event: {
          name: reservation.eventname,
          date: reservation.date,
          description: reservation.event_description,
          space_number: reservation.space_number,
          capacity: reservation.capacity
        },
        user: {
          name: reservation.username
        }
      };
    });
    console.log(reservationsWithSpace);
    return reservationsWithSpace;
  }

  async updateReservation (reservation: ReservationProps): Promise<ReservationProps> {
    const ReservationDao = ReservationMapper.toDao(reservation);
    const query = 'UPDATE public."Reservations" SET state = $1 WHERE id = $2 RETURNING *';
    const values = [
      ReservationDao.state,
      ReservationDao.created_at
    ];
    const result = await this.connection.query(query, values);
    return ReservationMapper.toEntity(result.rows[0]) as ReservationProps;
  }

  async deleteReservation (id: string): Promise<ReservationProps | null | undefined> {
    const query = 'DELETE FROM public."Reservations" WHERE id = $1';
    const result = await this.connection.query(query, [id]);
    return (result.rows[0] !== undefined && result.rows[0] !== null) ? ReservationMapper.toEntity(result.rows[0]) : null;
  }

  async setState (id: string, state: string): Promise<ReservationProps | null | undefined> {
    const query = 'UPDATE public."Reservations" SET state = $1 WHERE id = $2';
    const result = await this.connection.query(query, [state, id]);
    return (result.rows[0] !== undefined && result.rows[0] !== null) ? ReservationMapper.toEntity(result.rows[0]) : null;
  }

  async getAllReservations (): Promise<ReservationProps[] | null | undefined> {
    const query = 'SELECT * FROM public."Reservations"';
    const result = await this.connection.query(query);
    return result.rows.map(ReservationMapper.toEntity);
  }

  async addProduct (idReserve: string, idProduct: string): Promise<ReservationProps | null | undefined> {
    const query = 'Insert into public."ReservationsXProducts" (id_reserve, id_product) values ($1, $2)';
    const result = await this.connection.query(query, [idReserve, idProduct]);
    return (result.rows[0] !== undefined && result.rows[0] !== null) ? ReservationMapper.toEntity(result.rows[0]) : null;
  }
}
