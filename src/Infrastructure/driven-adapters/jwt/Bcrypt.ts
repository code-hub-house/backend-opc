import bcrypt from 'bcrypt';

export class Bcrypt {
  saltRounds: number;
  constructor (saltRounds: number) {
    this.saltRounds = saltRounds;
  }

  async hash (value: string): Promise<string> {
    return await bcrypt.hash(value, this.saltRounds);
  }

  async compare (value: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(value, hash);
  }

  async genSalt (): Promise<string> {
    return await bcrypt.genSalt(this.saltRounds);
  }
}
